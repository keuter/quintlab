# Quintlab

![QR Code](img/qrkeutergitlab.png)

Lab for Quint related stuff.

Starting of the first section will be for the Cloud demo's. This includes the setup for for:
* Serverless FaaS demo
* Cloud Native Kubernetes Demo
* Gitlab demo
* ...

## Serverless FaaS
Currently based on Google Functions, a 5 minute hands-on demo to demonstrate the ease of setting up a simple service which reactes to outside events by triggering the service.

## Cloud Native Kubernets Demo
Currently also based on the Google Kubernetes Engine, which is to date the most stable and easy to implement Kubernetes environment.

## Gitlab demo
Showing of how well Cloud intgrates with DevOps style development lifecycle. This is in prepration of showing of strategic worth of CLoud for every Business Driven approach, others such drivers will be a Data driven approach based on Cloud capabilities.